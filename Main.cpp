#include <iostream>
#include <time.h>


int main()
{
	struct tm buf;
	time_t t = time(NULL);
	localtime_s(&buf,&t);
	//std::cout << buf.tm_mday;

	const int N = 6;
	int array[N][N];

	for (int i = 0; i < N; ++i) 
	{	
		for (int j = 0; j < N; ++j) 
		{	
			array[i][j] = i + j;
			std::cout << array[i][j] << " ";
		}
		std::cout << '\n';
	}
	std::cout << '\n';

	int LineNumber = 0;
	LineNumber = buf.tm_mday % N;
	int LineSum = 0;
	for (int j = 0; j < N; ++j) 
	{
		LineSum = LineSum + array[LineNumber][j];
	}
	std::cout << "Today is " << buf.tm_mday << "th.So line number " << LineNumber 
		<< ". \nAnd Sum of elements is " << LineSum << ".\n";
}
